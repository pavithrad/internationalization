function changeLanguage() {
    var language = document.getElementById("language").value;
   
    var paraOne, paraTwo;
    var head, about, contact; 
    var headval;
    switch (language) {

        case "English":
            window.alert("You have selected English");
            head="Home";
            about="About";
            contact="Contact";
            headval="Internationalization";
            paraOne="In computing, internationalization and localization are means of adapting computer software to different languages, regional differences and technical requirements of a target locale. Internationalization is the process of designing a software application so that it can be adapted to various languages and regions without engineering changes. Localization is the process of adapting internationalized software for a specific region or language by adding locale-specific components and translating text. Localization (which is potentially performed multiple times, for different locales) uses the infrastructure or flexibility provided by internationalization (which is ideally performed only once, or as an integral part of ongoing development)";
            paraTwo="Enabling code to support local, regional, language, or culturally related preferences. Typically this involves incorporating predefined localization data and features derived from existing libraries or user preferences. Examples include date and time formats, local calendars, number formats and numeral systems, sorting and presentation of lists, handling of personal names and forms of address, etc.Separating localizable elements from source code or content, such that localized alternatives can be loaded or selected based on the user's international preferences as needed. Internationalization significantly affects the ease of the product's localization. Retrofitting a linguistically- and culturally-centered deliverable for a global market is obviously much more difficult and time-consuming than designing a deliverable with the intent of presenting it globally.So ideally, internationalization occurs as a fundamental step in the design and development process, rather than as an afterthought that can often involve awkward and expensive re-engineering.";
            break;

        case "French":
            window.alert("You have selected French");
            head="maison";
            about="Sur";
            contact="Contac";
            headval="Internationalisation"
            paraOne = "En informatique, l'internationalisation et la localisation sont des moyens d'adapter les logiciels à différentes langues, les différences régionales et les exigences techniques d'une région cible . L'internationalisation est le processus de conception d'une application logicielle qui peut être adaptée à différentes langues et régions sans modification technique. La localisation est le processus d'adaptation du logiciel internationalisé pour une région ou une langue spécifique en ajoutant des composants spécifiques aux paramètres régionaux et en traduisant du texte. La localisation (qui est potentiellement effectuée plusieurs fois, pour différents environnements locaux) utilise l'infrastructure ou la flexibilité fournie par l'internationalisation (ce qui est idéalement effectué une seule fois, ou comme partie intégrante du développement en cours)";
            paraTwo = "Activation du code pour prendre en charge les préférences locales, régionales, linguistiques ou culturelles. En règle générale, cela implique l'intégration de données de localisation prédéfinies et des fonctionnalités dérivées des bibliothèques existantes ou des préférences des utilisateurs. Les exemples incluent les formats de date et heure, les calendriers locaux, les formats numériques et les systèmes numériques, le tri et la présentation des listes, la gestion des noms personnels et des formes d'adresse, etc. Séparer les éléments localisables du code source ou du contenu, de sorte que les alternatives localisées peuvent être chargées ou sélectionnées en fonction des préférences internationales de l'utilisateur au besoin. L'internationalisation affecte de manière significative la facilité de localisation du produit. La modernisation d'un livrable axé sur la linguistique et la culture pour un marché mondial est évidemment beaucoup plus difficile et prend beaucoup de temps que la conception d'un livrable dans le but de le présenter globalement. Donc, idéalement, l'internationalisation se produit comme une étape fondamentale dans le processus de conception et de développement, plutôt que comme une réflexion tardive qui peut souvent impliquer une ingénierie irréductible et coûteuse.";

            break;
        case "Japanese":
            window.alert("You have selected Japanese");
            head="ホーム";
            about="約";
            contact="接触";
            headval="国際化";
            paraOne = "コンピューティングでは、国際化とローカリゼーションは、ターゲット言語の異なる言語、地域差、技術要件にコンピュータソフトウェアを適合させる手段です。国際化は、エンジニアリングの変更なしにさまざまな言語や地域に適応できるようにソフトウェアアプリケーションを設計するプロセスです。ローカライゼーションは、ロケール固有のコンポーネントを追加してテキストを翻訳することによって、特定の地域または言語用の国際化ソフトウェアを適合させるプロセスです。ローカリゼーション（異なるロケールで複数回実行される可能性があります）は、国際化によって提供されるインフラストラクチャーや柔軟性を使用します（理想的には一度だけ実行されるか、進行中の開発の不可欠な部分です）";
            paraTwo="地域、地域、言語、または文化に関連する環境設定をサポートするコードを有効にする。通常、これには、既定のローカリゼーションデータと、既存のライブラリまたはユーザー設定から派生した機能を組み込むことが含まれます。例には、日付と時刻の形式、ローカルのカレンダー、数字の形式と数字のシステム、リストの並べ替えと表示、個人の名前と住所の形式などが含まれます。ローカライズされた要素を必要に応じてユーザーの国際的な設定に基づいてロードまたは選択できるように、ローカライズ可能な要素をソースコードまたはコンテンツから分離する。国際化は、製品のローカライゼーションの容易さに大きな影響を与えます。グローバル市場向けに言語文化的および文化的中心の成果物を刷新することは、それを世界的に提示するという目的で成果物を設計するよりはるかに困難で時間がかかることは明らかです。理想的には、国際化は、しばしば扱いにくくて高価なリエンジニアリングを伴うことがある補足としてではなく、設計と開発プロセスの基本的なステップとして発生します。";
            break;
        case "Chinese":
            window.alert("You have selected Chinese");
            head="家";
            about="关于";
            contact="联系";
            headval="国际化";
            paraOne = "在計算中，國際化和本土化是使計算機軟件適應不同語言，區域差異和目標語言環境的技術要求的手段。國際化是設計軟件應用程序的過程，使其可以適應各種語言和區域而不進行工程變更。本地化是通過添加區域特定組件和翻譯文本來適應特定區域或語言的國際化軟件的過程。本地化（可能針對不同地區實施多次）使用國際化提供的基礎架構或靈活性（理想情況下只執行一次，或作為正在進行的開發的組成部分）";
            paraTwo = "启用代码来支持本地，区域，语言或文化相关的偏好。通常这涉及到结合预定义的定位数据和从现有库或用户偏好导出的特征。示例包括日期和时间格式，本地日历，数字格式和数字系统，列表的排序和列表，处理个人姓名和地址形式等。将本地化元素与源代码或内容分离，以便可以根据需要根据用户的国际偏好来加载或选择本地化替代。国际化显着影响产品本地化的便利性。在全球市场上改革以语言和文化为中心的交付项目显然比设计可交付成果更加困难和耗时，目的是在全球呈现。因此，理想情况下，国际化是设计和开发过程中的一个基本步骤，而不是经常涉及笨重和昂贵的重新设计的事后思考。";
            break;
        case "Greek":
            window.alert("You have selected Greek");
            head="Σπίτι";
            about="σχετικά με";
            contact="Επικοινωνία";
            headval="Διεθνοποίηση";
            paraOne= "Στον τομέα της πληροφορικής, η διεθνοποίηση και ο εντοπισμός είναι μέσα προσαρμογής του λογισμικού ηλεκτρονικών υπολογιστών σε διαφορετικές γλώσσες, περιφερειακές διαφορές και τεχνικές απαιτήσεις ενός τοπικού στόχου. Η διεθνοποίηση είναι η διαδικασία σχεδιασμού μιας εφαρμογής λογισμικού, ώστε να μπορεί να προσαρμοστεί σε διάφορες γλώσσες και περιοχές χωρίς μηχανικές αλλαγές. Η τοπική προσαρμογή είναι η διαδικασία προσαρμογής του διεθνοποιημένου λογισμικού σε μια συγκεκριμένη περιοχή ή γλώσσα, με την προσθήκη συστατικών για τοπικά στοιχεία και μετάφρασης κειμένου. Ο εντοπισμός (ο οποίος μπορεί να εκτελεστεί πολλές φορές για διαφορετικούς τόπους) χρησιμοποιεί την υποδομή ή την ευελιξία που παρέχει η διεθνοποίηση (η οποία πραγματοποιείται ιδανικά μόνο μία φορά ή ως αναπόσπαστο μέρος της συνεχιζόμενης ανάπτυξης)";
            paraTwo="Ενεργοποίηση κώδικα για την υποστήριξη τοπικών, περιφερειακών, γλωσσικών ή πολιτισμικών προτιμήσεων. Τυπικά, αυτό περιλαμβάνει την ενσωμάτωση προκαθορισμένων δεδομένων εντοπισμού και χαρακτηριστικών που προέρχονται από υπάρχουσες βιβλιοθήκες ή προτιμήσεις χρηστών.Παραδείγματα περιλαμβάνουν μορφές ημερομηνίας και ώρας, τοπικά ημερολόγια, μορφές αριθμών και αριθμητικά συστήματα, ταξινόμηση και παρουσίαση λιστών, χειρισμός προσωπικών ονομάτων και μορφών διεύθυνσης κ.λπ.Διαχωρίζοντας εντοπισμένα στοιχεία από τον πηγαίο κώδικα ή το περιεχόμενο, έτσι ώστε να μπορούν να φορτωθούν ή να επιλεγούν τοπικές εναλλακτικές λύσεις με βάση τις διεθνείς προτιμήσεις του χρήστη, όπως απαιτείται. Η διεθνοποίηση επηρεάζει σημαντικά την ευκολία του εντοπισμού του προϊόντος.Η μετασκευή ενός γλωσσολογικά και πολιτισμικά προσανατολισμένου προϊόντος σε μια παγκόσμια αγορά είναι προφανώς πολύ πιο δύσκολο και χρονοβόρο από το σχεδιασμό ενός παραδοτέου με σκοπό να το παρουσιάσουμε παγκοσμίως. Έτσι, στην ιδανική περίπτωση, η διεθνοποίηση εμφανίζεται ως ένα θεμελιώδες βήμα στη διαδικασία σχεδιασμού και ανάπτυξης, και όχι ως μια δεύτερη σκέψη που μπορεί συχνά να συνεπάγεται αμήχανη και δαπανηρή αναδιάρθρωση.";
            break;
        case "Hindi":
            window.alert("You have selected Hindi");
            head="घर";
            about="के बारे में";
            contact="संपर्क करें";
            headval="अंतर्राष्ट्रीयकरण";
            paraOne = "कंप्यूटिंग, अंतर्राष्ट्रीयकरण और स्थानीयकरण में कंप्यूटर सॉफ़्टवेयर को अलग-अलग भाषाओं, क्षेत्रीय मतभेदों और लक्षित लोकेल की तकनीकी आवश्यकताओं के अनुकूल बनाने का मतलब है। अंतर्राष्ट्रीयकरण एक सॉफ़्टवेयर एप्लिकेशन को डिजाइन करने की प्रक्रिया है ताकि इसे इंजीनियरिंग परिवर्तन के बिना विभिन्न भाषाओं और क्षेत्रों में अनुकूलित किया जा सके। स्थानीयकरण, लोकेल-विशिष्ट घटकों को जोड़कर और टेक्स्ट का अनुवाद करके एक विशिष्ट क्षेत्र या भाषा के लिए अंतर्राष्ट्रीयकृत सॉफ़्टवेयर को आदत डालने की प्रक्रिया है। स्थानीयकरण (जो अलग-अलग स्थानों के लिए कई बार संभावित रूप से किया जाता है) अंतर्राष्ट्रीयकरण द्वारा प्रदान की जाने वाली अवसंरचना या लचीलेपन का उपयोग करता है (जो आदर्श रूप से केवल एक बार, या चल रहे विकास का एक अभिन्न हिस्सा है)";
            paraTwo ="स्थानीय, क्षेत्रीय, भाषा या सांस्कृतिक रूप से संबंधित प्राथमिकताओं का समर्थन करने के लिए कोड सक्षम करना आमतौर पर इसमें पूर्वनिर्धारित स्थानीयकरण डेटा और मौजूदा पुस्तकालयों या उपयोगकर्ता वरीयताओं से प्राप्त सुविधाओं को शामिल करना शामिल है उदाहरणों में दिनांक और समय प्रारूप, स्थानीय कैलेंडर, संख्या स्वरूप और अंक प्रणाली, सूचियों की छंटाई और प्रस्तुति, व्यक्तिगत नामों के पते और पते के रूप आदि शामिल हैं। स्रोत कोड या सामग्री के स्थानीयकरण योग्य तत्वों को अलग करना, जैसे कि स्थानीय विकल्प को उपयोगकर्ता की अंतर्राष्ट्रीय प्राथमिकताओं के आधार पर लोड या चुना जा सकता है। अंतर्राष्ट्रीयकरण उत्पाद के स्थानीयकरण की आसानी से प्रभावित करता है |वैश्विक बाजार के लिए एक भाषायी और सांस्कृतिक-केन्द्रित वितरित किए जाने वाले रिट्रोफाईटिंग, विश्व स्तर पर इसे प्रस्तुत करने के इरादे से डिलीवर करने के लिए डिजाइन करने की तुलना में अधिक कठिन और समय-उपभोक्ता है। इसलिए आदर्श रूप में, अंतर्राष्ट्रीयकरण को डिजाइन और विकास प्रक्रिया में एक मौलिक कदम के रूप में देखा जाता है, जो कि बाद में सोचने की बजाय अक्सर अजीब और महंगी पुन: इंजीनियरिंग शामिल हो सकता है।"
            break;
        
        case "Spanish":
            window.alert("You have selected Spanish");
            head="casa";
            about="acerca de";
            contact="contacto";
            headval="Internacionalización";
            paraOne="En la informática, la internacionalización y la localización son medios de adaptar el software a diferentes idiomas, las diferencias regionales y los requisitos técnicos de una localidad de destino.La internacionalización es el proceso de diseñar una aplicación de software para que pueda adaptarse a varios idiomas y regiones sin cambios de ingeniería. La localización es el proceso de adaptación de software internacionalizado para una región o un idioma específico mediante la adición de componentes específicos de la configuración regional y la traducción de texto. La localización (que se realiza potencialmente varias veces, para diferentes locales) utiliza la infraestructura o flexibilidad proporcionada por la internacionalización (que se realiza idealmente sólo una vez o como parte integral del desarrollo en curso) ";
            paraTwo="Habilitación de código para respaldar las preferencias locales, regionales, lingüísticas o culturalmente relacionadas. Normalmente, esto implica la incorporación de datos de localización predefinidos y características derivadas de bibliotecas existentes o preferencias de usuario. Los ejemplos incluyen formatos de fecha y hora, calendarios locales, formatos numéricos y sistemas numéricos, clasificación y presentación de listas, manejo de nombres personales y formas de dirección, etc. Separación de elementos localizables del código fuente o contenido, de modo que se puedan cargar alternativas localizadas o seleccionados según las preferencias internacionales del usuario según sea necesario. La internacionalización afecta significativamente la facilidad de localización del producto. La adaptación de un producto lingüístico y culturalmente centrado para un mercado global es obviamente mucho más difícil y requiere mucho tiempo que diseñar un producto con la intención de presentarlo globalmente. Así que idealmente, la internacionalización se da como un paso fundamental en el proceso de diseño y desarrollo, más que como una idea tardía que a menudo puede implicar reingeniería incómoda y costosa.";
            break;
        default:
            window.alert("You have selected to apply default language");
            head="Home";
            about="About";
            contact="Contact";
            headval="Internationalization";
            paraOne="In computing, internationalization and localization are means of adapting computer software to different languages, regional differences and technical requirements of a target locale. Internationalization is the process of designing a software application so that it can be adapted to various languages and regions without engineering changes. Localization is the process of adapting internationalized software for a specific region or language by adding locale-specific components and translating text. Localization (which is potentially performed multiple times, for different locales) uses the infrastructure or flexibility provided by internationalization (which is ideally performed only once, or as an integral part of ongoing development)";
            paraTwo="Enabling code to support local, regional, language, or culturally related preferences. Typically this involves incorporating predefined localization data and features derived from existing libraries or user preferences. Examples include date and time formats, local calendars, number formats and numeral systems, sorting and presentation of lists, handling of personal names and forms of address, etc.Separating localizable elements from source code or content, such that localized alternatives can be loaded or selected based on the user's international preferences as needed. Internationalization significantly affects the ease of the product's localization. Retrofitting a linguistically- and culturally-centered deliverable for a global market is obviously much more difficult and time-consuming than designing a deliverable with the intent of presenting it globally.So ideally, internationalization occurs as a fundamental step in the design and development process, rather than as an afterthought that can often involve awkward and expensive re-engineering.";
            break;

    }
   //set the innerHtml property to the specific content
    document.getElementById("home").innerHTML = head;
    document.getElementById("about").innerHTML = about;
    document.getElementById("contact").innerHTML = contact;
    document.getElementById("head").innerHTML = headval;
    document.getElementById("output").innerHTML = paraOne;
    document.getElementById("outputTwo").innerHTML = paraTwo;
    
   
}